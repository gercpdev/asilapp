package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import it.uniba.sms.gercp.asilapp.R;

public class InformazioneActivity extends AppCompatActivity {

    final String URLDcumenti_cda = "gs://asilapp-1bb2e.appspot.com/documenti/informazione/cda/";
    final String URLDcumenti_glossario = "gs://asilapp-1bb2e.appspot.com/documenti/informazione/glossario/";
    final String URLDcumenti_guidaMigranti = "gs://asilapp-1bb2e.appspot.com/documenti/informazione/guidaMigranti/";

    private Button cda1,cda2,cda3,cda4,cda5,cda6,glossario,guida1,guida2;
    private ProgressBar progressBar1, progressBar2, progressBar3,progressBar4, progressBar5, progressBar6,
            progressBar7, progressBar8, progressBar9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informazione);

        getWindow().setEnterTransition(new Explode());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        cda1 = findViewById(R.id.cda1Butt);
        cda2 = findViewById(R.id.cda2Butt);
        cda3 = findViewById(R.id.cda3Butt);
        cda4 = findViewById(R.id.cda4Butt);
        cda5 = findViewById(R.id.cda5Butt);
        cda6 = findViewById(R.id.cda6Butt);
        glossario = findViewById(R.id.glossarioButt);
        guida1 = findViewById(R.id.guida1Butt);
        guida2 = findViewById(R.id.guida2Butt);
        progressBar1 = findViewById(R.id.cda1ProgressBar);
        progressBar2 = findViewById(R.id.cda2ProgressBar);
        progressBar3 = findViewById(R.id.cda3ProgressBar);
        progressBar4 = findViewById(R.id.cda4ProgressBar);
        progressBar5 = findViewById(R.id.cda5ProgressBar);
        progressBar6 = findViewById(R.id.cda6ProgressBar);
        progressBar7 = findViewById(R.id.glossarioProgressBar);
        progressBar8 = findViewById(R.id.guida1ProgressBar);
        progressBar9 = findViewById(R.id.guida2ProgressBar);


        cda1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar1.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_cda,"CDA_1.pdf");
            }
        });

        cda2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar2.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_cda,"CDA_2.pdf");
            }
        });

        cda3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar3.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_cda,"ISCRIZIONE_SSN.pdf");
            }
        });

        cda4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar4.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_cda,"MEDICAL_ASSISTANCE.pdf");
            }
        });

        cda5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar5.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_cda,"REGOLAMENTO.pdf");
            }
        });

        cda6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar6.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_cda,"SPRAR.pdf");
            }
        });

        glossario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar7.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_glossario,"glossario.pdf");
            }
        });

        guida1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar8.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_guidaMigranti,"GUIDA_ITA.pdf");
            }
        });

        guida2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar9.setVisibility(View.VISIBLE);
                downloadFile(URLDcumenti_guidaMigranti,"GUIDA_ARABO.pdf");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadFile(String URL,String file_name) {
        StorageReference storage = FirebaseStorage.getInstance().getReferenceFromUrl(URL);
        StorageReference target_file = storage.child(file_name);

        File rootPath = new File(Environment.getExternalStorageDirectory()+"/AsilApp/", "Informazione docs");
        if(!rootPath.exists()) {
            rootPath.mkdirs();
        }

        Log.d("VERIFYPATH",rootPath.getAbsolutePath());

        final File localFile = new File(rootPath,file_name);


        target_file.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Snackbar.make(findViewById(R.id.activity_informazione),"Salvato in "+localFile.getAbsolutePath(),Snackbar.LENGTH_LONG).show();
                        progressBar1.setVisibility(View.INVISIBLE);
                        progressBar2.setVisibility(View.INVISIBLE);
                        progressBar3.setVisibility(View.INVISIBLE);
                        progressBar4.setVisibility(View.INVISIBLE);
                        progressBar5.setVisibility(View.INVISIBLE);
                        progressBar6.setVisibility(View.INVISIBLE);
                        progressBar7.setVisibility(View.INVISIBLE);
                        progressBar8.setVisibility(View.INVISIBLE);
                        progressBar9.setVisibility(View.INVISIBLE);
                    }})
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Snackbar.make(findViewById(R.id.activity_informazione),"Impossibile scaricare",Snackbar.LENGTH_LONG).show();
                        progressBar1.setVisibility(View.INVISIBLE);
                        progressBar2.setVisibility(View.INVISIBLE);
                        progressBar3.setVisibility(View.INVISIBLE);
                        progressBar4.setVisibility(View.INVISIBLE);
                        progressBar5.setVisibility(View.INVISIBLE);
                        progressBar6.setVisibility(View.INVISIBLE);
                        progressBar7.setVisibility(View.INVISIBLE);
                        progressBar8.setVisibility(View.INVISIBLE);
                        progressBar9.setVisibility(View.INVISIBLE);
                    }})
                .addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressBar1.setProgress((int)progress);
                        progressBar2.setProgress((int)progress);
                        progressBar3.setProgress((int)progress);
                        progressBar4.setProgress((int)progress);
                        progressBar5.setProgress((int)progress);
                        progressBar6.setProgress((int)progress);
                        progressBar7.setProgress((int)progress);
                        progressBar8.setProgress((int)progress);
                        progressBar9.setProgress((int)progress);
                    }});
    }
}
