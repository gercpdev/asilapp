package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria;

public class SchedaSanitariaClass {

    private String dataCompilazione;
    private String temperatura;
    private String peso;
    private String pressione_minima;
    private String pressione_massima;
    private String glicemia;
    private String respiro;

    public SchedaSanitariaClass(){

    }

    public SchedaSanitariaClass(String dataCompilazione, String temperatura, String peso,
                                String pressione_minima, String pressione_massima, String glicemia, String respiro) {
        this.dataCompilazione = dataCompilazione;
        this.temperatura = temperatura;
        this.peso = peso;
        this.pressione_minima = pressione_minima;
        this.pressione_massima = pressione_massima;
        this.glicemia = glicemia;
        this.respiro = respiro;
    }

    public String getData() {
        return dataCompilazione;
    }

    public void setData(String data) {
        this.dataCompilazione = data;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getPressione_minima() {
        return pressione_minima;
    }

    public void setPressione_minima(String pressione_minima) {
        this.pressione_minima = pressione_minima;
    }

    public String getPressione_massima() {
        return pressione_massima;
    }

    public void setPressione_massima(String pressione_massima) {
        this.pressione_massima = pressione_massima;
    }

    public String getGlicemia() {
        return glicemia;
    }

    public void setGlicemia(String glicemia) {
        this.glicemia = glicemia;
    }

    public String getRespiro() {
        return respiro;
    }

    public void setRespiro(String respiro) {
        this.respiro = respiro;
    }

}
