package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.centroAccoglienza;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import it.uniba.sms.gercp.asilapp.R;
import it.uniba.sms.gercp.asilapp.chat_handler.ChatActivity;

public class CentroAccoglienza extends Fragment {

    private ImageView immagine;
    private FloatingActionButton fab_call, fab_chat, fab_expand;
    private ProgressBar progressBar;
    private TextView nome_indirizzo, anno, posti, ente, servizi, descrizione;
    private ArrayList<Uri> gallery = new ArrayList<>();
    private Handler handler;
    private Runnable runnable;
    private Animation fadingINAnimation;
    public String num_telefono_fab;
    private boolean isFABOpen = false;
    View viewForeground;
    DatabaseReference mDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.centro_accoglienza, container, false);
        fadingINAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fading_anim);

        immagine = rootView.findViewById(R.id.collapseImageView);
        immagine.setAnimation(fadingINAnimation);
        fab_call = rootView.findViewById(R.id.fab_telephone);
        fab_chat = rootView.findViewById(R.id.fab_chat);
        fab_expand = rootView.findViewById(R.id.fab_expand);
        nome_indirizzo = rootView.findViewById(R.id.nome_inidirizzo);
        anno = rootView.findViewById(R.id.anno);
        posti  = rootView.findViewById(R.id.posti);
        ente  = rootView.findViewById(R.id.ente);
        servizi  = rootView.findViewById(R.id.servizi);
        descrizione  = rootView.findViewById(R.id.descrizione);
        progressBar = rootView.findViewById(R.id.progressCDA);
        viewForeground = rootView.findViewById(R.id.spiceCoordinatorLayoutId);

        /* Animazioni sul FAB group */
        final AnimatorSet rotate = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(),
                R.animator.rotate_fab);
        final AnimatorSet rotate_off = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(),
                R.animator.rotate_fab_inverse);
        final AnimatorSet show_fab = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(),
                R.animator.show_fab);
        final AnimatorSet hide_fab = (AnimatorSet) AnimatorInflater.loadAnimator(getContext(),
                R.animator.hide_fab);

        fab_chat.setScaleX(0);
        fab_chat.setScaleY(0);
        fab_call.setScaleX(0);
        fab_call.setScaleY(0);
        Drawable foreground = ContextCompat.getDrawable(getContext(), R.drawable.foreground);
        foreground.setAlpha(0);
        viewForeground.setForeground(foreground);

        gallery_image();
        caricaInfoCDA();


        fab_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){

                    showFABMenu(rotate, show_fab, viewForeground);
                }else{
                    closeFABMenu(rotate_off, hide_fab, viewForeground);
                }
            }
        });

        fab_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", num_telefono_fab, null));
                startActivity(intent);
            }
        });

        fab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chat = new Intent(getActivity().getApplicationContext(), ChatActivity.class);
                startActivity(chat, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });


        return rootView;
    }

    private void showFABMenu(AnimatorSet rotate, AnimatorSet show_fab, View viewForeground){
        isFABOpen = true;
        ObjectAnimator animator = ObjectAnimator.ofInt(viewForeground.getForeground(), "alpha", 0, 255);
        animator.setDuration(80L);
        AnimatorSet show_fab2 = show_fab.clone();
        rotate.setTarget(fab_expand);
        show_fab.setTarget(fab_call);
        show_fab2.setTarget(fab_chat);
        show_fab2.setStartDelay(60L);
        animator.start();
        rotate.start();
        show_fab.start();
        show_fab2.start();
    }

    private void closeFABMenu(AnimatorSet rotate_off, AnimatorSet hide_fab, View viewForeground){
        isFABOpen=false;
        ObjectAnimator animator = ObjectAnimator.ofInt(viewForeground.getForeground(), "alpha", 255, 0);
        animator.setDuration(70L);
        AnimatorSet hide_fab2 = hide_fab.clone();
        rotate_off.setTarget(fab_expand);
        hide_fab.setTarget(fab_chat);
        hide_fab2.setTarget(fab_call);
        hide_fab2.setStartDelay(100L);
        animator.start();
        rotate_off.start();
        hide_fab.start();
        hide_fab2.start();
    }

    private void scaricaImmagini(){
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        storageReference.child("images/fotoCentroAccoglienza/pic1.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        gallery.add(uri);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(getView(),"Connection Error", Snackbar.LENGTH_LONG).show();
                    }
                });
        storageReference.child("images/fotoCentroAccoglienza/pic2.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        gallery.add(uri);
                    }
                });

        storageReference.child("images/fotoCentroAccoglienza/pic3.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        gallery.add(uri);
                    }
                });

        storageReference.child("images/fotoCentroAccoglienza/pic4.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        gallery.add(uri);
                    }
                });
        storageReference.child("images/fotoCentroAccoglienza/pic5.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        gallery.add(uri);
                    }
                });

        storageReference.child("images/fotoCentroAccoglienza/pic6.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        gallery.add(uri);
                    }
                });


        progressBar.setVisibility(View.INVISIBLE);
    }

    private void gallery_image(){

        scaricaImmagini();
        handler = new Handler();
        runnable = new Runnable() {
            int i=0;
            public void run() {

                if(!gallery.isEmpty()) {
                    immagine.startAnimation(fadingINAnimation);
                    Glide.with(getActivity())
                            .load(gallery.get(i).toString())
                            .into(immagine);
                    Log.d("URIARRAY", gallery.toString());

                    i++;
                    if (i > gallery.size() - 1) {
                        i = 0;
                    }

                    handler.postDelayed(this, 3500);
                }

            }
        };

        handler.postDelayed(runnable, 3500);
    }

    private void caricaInfoCDA(){

        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_CDA));
        mDatabase.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                DataSnapshot child_dataSnapshot = dataSnapshot.child(getString(R.string.id_centro));
                CentroAccoglienzaClass cda = child_dataSnapshot.getValue(CentroAccoglienzaClass.class);

                if(cda!=null){
                    Log.d("CDAPROVA","Lettura effettuata");
                }

                nome_indirizzo.setText(cda.getNome_centro()+"\n"+cda.getIndirizzo()+"\n"+cda.getCap()+
                        " "+cda.getCitta()+"\n"+cda.getMail()+" Tel."+cda.getNum_telefonoCDA());
                anno.setText(cda.getAnno());
                posti.setText(cda.getPosti());
                ente.setText(cda.getEnte_gestore());
                servizi.setText(cda.getServizi());
                descrizione.setText(cda.getDescrizione());
                num_telefono_fab = cda.getNum_telefonoCDA();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Snackbar.make(getView(),"Connection error",Snackbar.LENGTH_LONG).show();
            }
        });
    }

    // Ferma il thread
    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
