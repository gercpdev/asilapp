package it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Fade;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.support.design.widget.Snackbar;

import it.uniba.sms.gercp.asilapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private TextView app_name;
    private ImageView logo;
    private EditText nome,cognome,email,password,confirm_password;
    private RadioButton maschio,femmina;
    private Button register;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getWindow().setEnterTransition(new Fade());
        getWindow().setExitTransition(new Fade());

        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti));
        mAuth = FirebaseAuth.getInstance();

        app_name = findViewById(R.id.appName);
        logo = findViewById(R.id.logo);
        nome = findViewById(R.id.input_nome);
        cognome = findViewById(R.id.input_cognome);
        maschio = findViewById(R.id.maleRadio);
        femmina = findViewById(R.id.femaleRadio);
        email = findViewById(R.id.email_input);
        password = findViewById(R.id.password);

        Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotation_anim);
        Animation startFadeAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fading_anim_offset);
        logo.startAnimation(startRotateAnimation);
        app_name.startAnimation(startFadeAnimation);

        confirm_password = findViewById(R.id.password2);

        // Se viene cliccato il pulsante Registrati nella tastiera virtuale
        confirm_password.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    attemptRegister();
                    handled = true;
                }
                return handled;
            }
        });

        register = findViewById(R.id.registra_butt);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
                            }
        });
    }

    // Se viene cliccato il bottone registrati
    private void attemptRegister() {

        // Reset errors.
        email.setError(null);
        password.setError(null);
        confirm_password.setError(null);
        nome.setError(null);
        cognome.setError(null);

        String email = this.email.getText().toString();
        String password = this.password.getText().toString();
        String conf_password = confirm_password.getText().toString();
        String nome = this.nome.getText().toString();
        String cognome = this.cognome.getText().toString();
        String genere = "";

        View focusView = null;
        boolean cancel = false;

        if (maschio.isChecked())
            genere = "M";
        if (femmina.isChecked())
            genere = "F";

        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            this.password.setError(getString(R.string.error_password_length));
            focusView = this.password;
            cancel = true;
        } else if (TextUtils.isEmpty(conf_password) || !isPasswordValid(conf_password) || !password.equals(conf_password)) {
                confirm_password.setError(getString(R.string.error_password_match));
                focusView = this.confirm_password;
                cancel = true;
        }

        if (TextUtils.isEmpty(email) || !isEmailValid(email) ) {
            this.email.setError(getString(R.string.error_field_required));
            focusView = this.email;
            cancel = true;
        }

        if (TextUtils.isEmpty(cognome)) {
            this.cognome.setError(getString(R.string.error_field_required));
            focusView = this.cognome;
            cancel = true;
        }

        if (TextUtils.isEmpty(nome)) {
            this.nome.setError(getString(R.string.error_field_required));
            focusView = this.nome;
            cancel = true;
        }


        if (cancel){
            focusView.requestFocus();
        } else {
            registerAccount(email,password,nome,cognome,genere);
        }

    }

    public void registerAccount( final String email, final String password, final String nome,
                                final String cognome, final String sesso){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            registerNewUser(mAuth.getUid(),email,password,nome,cognome,sesso);
                            Snackbar.make(findViewById(R.id.activity_register),R.string.registered_ok,Snackbar.LENGTH_LONG).show();
                            sendVerificationEmail();
                        } else {
                            //SE LA MAIL E' GIA' ESISTENTE NON PUO' REGISTRARSI
                            Snackbar.make(findViewById(R.id.activity_register),R.string.registered_fail,Snackbar.LENGTH_LONG).show();

                        }
                    }
                });
    }
    public void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // email sent
                            FirebaseAuth.getInstance().signOut();
                            Snackbar.make(findViewById(R.id.activity_register),R.string.send_email_ok,Snackbar.LENGTH_LONG).show();
                            Intent returnLogin = new Intent(getApplicationContext(),LoginActivity.class);
                            startActivity(returnLogin, ActivityOptions.makeSceneTransitionAnimation(RegisterActivity.this).toBundle());
                            finish();
                        }
                    }
                });
    }

    public void registerNewUser(String id, String mail, String password, String nome, String cognome, String sesso){
        RichiedenteAsilo new_user = new RichiedenteAsilo(id,mail,password,nome,cognome,sesso);
        new_user.setData_nascita("");
        new_user.setCitta_provenienza("");
        new_user.setStato_provenienza("");
        new_user.setNum_telefono("");
        new_user.setIMG_URL("");
        mDatabase.child(id).setValue(new_user);
        mAuth.createUserWithEmailAndPassword(mail,password);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    //Controlla se la password è più lunga di 4 caratteri
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}
