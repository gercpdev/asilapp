package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.centroAccoglienza;

public class CentroAccoglienzaClass {
    private String nome_centro;
    private String indirizzo;
    private String citta;
    private String cap;
    private String num_telefonoCDA;
    private String mail;
    private String anno;
    private String posti;
    private String ente_gestore;
    private String servizi;
    private String descrizione;

    public CentroAccoglienzaClass(){
        //essenziale per la lettura Datasnapshot
    }

    public CentroAccoglienzaClass(String nome_centro, String indirizzo, String citta, String cap,
                                  String num_telefono, String mail, String anno, String posti, String ente_gestore,
                                  String servizi, String descrizione) {
        this.nome_centro = nome_centro;
        this.indirizzo = indirizzo;
        this.citta = citta;
        this.cap = cap;
        this.num_telefonoCDA = num_telefono;
        this.mail = mail;
        this.anno = anno;
        this.posti = posti;
        this.ente_gestore = ente_gestore;
        this.servizi = servizi;
        this.descrizione = descrizione;
    }

    public String getNome_centro() {
        return nome_centro;
    }

    public void setNome_centro(String nome_centro) {
        this.nome_centro = nome_centro;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getNum_telefonoCDA() {
        return num_telefonoCDA;
    }

    public void setNum_telefono(String num_telefono) {
        this.num_telefonoCDA = num_telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getPosti() {
        return posti;
    }

    public void setPosti(String posti) {
        this.posti = posti;
    }

    public String getEnte_gestore() {
        return ente_gestore;
    }

    public void setEnte_gestore(String ente_gestore) {
        this.ente_gestore = ente_gestore;
    }

    public String getServizi() {
        return servizi;
    }

    public void setServizi(String servizi) {
        this.servizi = servizi;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
}
