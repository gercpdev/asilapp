package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import it.uniba.sms.gercp.asilapp.R;

public class VideoDisponibiliDialog extends AppCompatActivity {

    final String v1 = "https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/documenti%2Fformazione%2FvideoTutorial%2FWelcome.mp4?alt=media&token=f4cfabc1-ea5c-445b-82b4-771617b9e6b9";
    final String v2 = "https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/documenti%2Fformazione%2FvideoTutorial%2FAccoglienza.mp4?alt=media&token=5a35aef9-8ba9-48a9-8d34-2ab4fac89fa5";
    final String v3 = "https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/documenti%2Fformazione%2FvideoTutorial%2FCittadinanza.mp4?alt=media&token=30f3058c-a6b3-442f-9aa0-c7c03b492293";
    final String v4 = "https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/documenti%2Fformazione%2FvideoTutorial%2FLavoro.mp4?alt=media&token=0ae500ee-1d73-4c85-8525-9cf6b088093a";
    final String v5 = "https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/documenti%2Fformazione%2FvideoTutorial%2FPercorsoGiuridico.mp4?alt=media&token=1b876055-1141-4323-9fec-b9a51567eb28";

    private Button video1, video2, video3, video4, video5;
    private VideoView mVideoView;
    private ProgressBar progressBarDialog;
    private MediaController mediaController;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_disponibili_dialog);

        mediaController = new MediaController(this);
        progressBarDialog = findViewById(R.id.progressBarDialog);
        mVideoView = (VideoView) findViewById(R.id.dialogVideoView);
        mVideoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mediaController != null) {
                    mediaController.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mediaController.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                return false;
            }
        });

        startVideo(v1);

        video1 = findViewById(R.id.welcome);
        video1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVideo(v1);
            }
        });

        video2 = findViewById(R.id.accoglienza);
        video2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVideo(v2);
            }
        });

        video3 = findViewById(R.id.cittadinanza);
        video3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVideo(v3);
            }
        });

        video4 = findViewById(R.id.lavoro);
        video4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVideo(v4);
            }
        });

        video5 = findViewById(R.id.percorsoGiuridico);
        video5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVideo(v5);
            }
        });
    }

    private void startVideo(String url_video) {

        mVideoView.setVideoURI(Uri.parse(url_video));
        mVideoView.requestFocus();

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        progressBarDialog.setVisibility(View.GONE);
                        mVideoView.setMediaController(mediaController);
                        mediaController.setAnchorView(mVideoView);
                        ((ViewGroup)mediaController.getParent()).removeView(mediaController);
                        ((FrameLayout) findViewById(R.id.videoViewWrapper)).addView(mediaController);
                        mediaController.setVisibility(View.INVISIBLE);
                    }
                });
                mVideoView.start();
            }
        });
    }
}
