package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.luoghiDintorni;

public class LuogoClass {
    private String nome_luogo;
    private String indirizzo;

    public LuogoClass(){

    }

    public LuogoClass(String nome_luogo, String indirizzo){
        this.nome_luogo = nome_luogo;
        this.indirizzo = indirizzo;
    }

    public String getNome_luogo(){
        return nome_luogo;
    }

    public String getIndirizzo(){
        return indirizzo;
    }
}
