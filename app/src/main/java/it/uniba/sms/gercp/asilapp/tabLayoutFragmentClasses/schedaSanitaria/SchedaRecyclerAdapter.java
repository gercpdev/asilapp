package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import it.uniba.sms.gercp.asilapp.R;

public class SchedaRecyclerAdapter extends RecyclerView.Adapter<SchedaRecyclerAdapter.MyHoder>{

    private List<SchedaSanitariaClass> list;
    private Context context;

    public SchedaRecyclerAdapter(List<SchedaSanitariaClass> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyHoder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.scheda_cardview,parent,false);
        MyHoder myHoder = new MyHoder(view);
        return myHoder;
    }

    @Override
    public void onBindViewHolder(MyHoder holder, int position) {
        SchedaSanitariaClass mylist = list.get(position);

        holder.dataCompilazione.setText(mylist.getData());
        holder.temperatura.setText(mylist.getTemperatura());
        holder.peso.setText(mylist.getPeso());
        holder.pressione.setText("min. : "+mylist.getPressione_minima()+ " max. : "+mylist.getPressione_massima());
        holder.glicemia.setText(mylist.getGlicemia());
        holder.respiro.setText(mylist.getRespiro());
    }

    @Override
    public int getItemCount() {
        int arr = 0;

        try{
            if(list.size()==0){
                arr = 0;
            }
            else{
                arr=list.size();
            }

        }catch (Exception e){

        }

        return arr;
    }

    class MyHoder extends RecyclerView.ViewHolder{
        TextView dataCompilazione, temperatura, peso, pressione, glicemia, respiro;


        public MyHoder(View itemView) {
            super(itemView);
            dataCompilazione = (TextView) itemView.findViewById(R.id.id_card);
            temperatura = (TextView) itemView.findViewById(R.id.temp_card);
            peso = (TextView) itemView.findViewById(R.id.peso_card);
            pressione = (TextView) itemView.findViewById(R.id.pressione_card);
            glicemia = (TextView) itemView.findViewById(R.id.glicemia_card);
            respiro = (TextView) itemView.findViewById(R.id.respiro_card);

        }
    }

}