package it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes;

public class RichiedenteAsilo {

    private String IMG_URL;
    private String id;
    private String mail;
    private String password;
    private String nome;
    private String cognome;
    private String sesso;
    private String data_nascita;
    private String citta_provenienza;
    private String stato_provenienza;
    private String num_telefono;
    private float votoApp;

    public RichiedenteAsilo(){
        //essenziale per la lettura Datasnapshot
    }

    public RichiedenteAsilo(String id, String mail, String password, String nome, String cognome, String sesso) {
        this.id = id;
        this.mail = mail;
        this.password = password;
        this.nome = nome;
        this.cognome = cognome;
        this.sesso = sesso;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getVotoApp() {
        return votoApp;
    }

    public void setVotoApp (float votoApp) {
        this.votoApp = votoApp;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSesso() {
        return sesso;
    }

    public String getData_nascita() {
        return data_nascita;
    }

    public void setData_nascita(String data_nascita) {
        this.data_nascita = data_nascita;
    }

    public String getCitta_provenienza() {
        return citta_provenienza;
    }

    public void setCitta_provenienza(String citta_provenienza) {
        this.citta_provenienza = citta_provenienza;
    }

    public String getStato_provenienza() {
        return stato_provenienza;
    }

    public void setStato_provenienza(String stato_provenienza) {
        this.stato_provenienza = stato_provenienza;
    }

    public String getNum_telefono() {
        return num_telefono;
    }

    public void setNum_telefono(String num_telefono) {
        this.num_telefono = num_telefono;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    // Utilizzato per LOG.d(..)
    public String toString(){
        return id+" "+nome+" "+cognome+" "+sesso+" ,"+data_nascita+" ,"+citta_provenienza+" ,"+stato_provenienza+
                " ,"+num_telefono+" "+votoApp;
}
    }
