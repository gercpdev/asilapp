package it.uniba.sms.gercp.asilapp.chat_handler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import java.util.Date;
import java.text.SimpleDateFormat;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;
import java.util.List;

import it.uniba.sms.gercp.asilapp.R;

public class ChatAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private static final String datePattern = "EEE, dd MMMM";
    private static final String timePattern = "HH:mm";

    private Context mContext;
    private List<Messaggio> mMessageList;
    private String userUID;
    private String centroAccoglienza;

    public ChatAdapter(Context context, List<Messaggio> messageList) {
        mContext = context;
        mMessageList = messageList;
        userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        centroAccoglienza = mContext.getString(R.string.id_centro);
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {

        Messaggio messaggioUtente = mMessageList.get(position);
        if (messaggioUtente.getMittente().equals(userUID)) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else if (messaggioUtente.getMittente().equals(centroAccoglienza)){
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
        return -1;
    }

    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Messaggio messaggioUtente = mMessageList.get(position);
        long previousTime = 0; // Rimane 0 se è il messaggio è il primo della lista
        if(position>1) previousTime = mMessageList.get(position - 1).getOrario();

        switch(holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(messaggioUtente, previousTime);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(messaggioUtente, previousTime);
                break;
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {

        TextView messageText, messageTime, messageDate;

        SentMessageHolder(View itemView) {
            super(itemView);
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            messageTime = (TextView) itemView.findViewById(R.id.text_message_time);
            messageDate = (TextView) itemView.findViewById(R.id.text_message_date);
        }

        void bind(Messaggio messaggio, long previousTime) {
            long currentTime = messaggio.getOrario();
            boolean dateChanged = isDateChanged(currentTime, previousTime);
            if(dateChanged) {
                CharSequence formattedDate = (String) DateFormat.format(datePattern,currentTime);
                messageDate.setVisibility(View.VISIBLE);
                messageDate.setText(formattedDate);
            }
            CharSequence formattedTime = (String) DateFormat.format(timePattern,currentTime);
            messageTime.setText(formattedTime);
            messageText.setText(messaggio.getMessaggio());
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        TextView messageText, messageTime, messageDate;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            messageTime = (TextView) itemView.findViewById(R.id.text_message_time);
            messageDate = (TextView) itemView.findViewById(R.id.text_message_date);
        }

        void bind(Messaggio messaggio, long previousTime) {
            long currentTime = messaggio.getOrario();
            Date fullDate = new Date(currentTime);
            boolean dateChanged = isDateChanged(currentTime, previousTime);
            if(dateChanged) {
                CharSequence formattedDate = (String) DateFormat.format(datePattern,fullDate);
                messageDate.setVisibility(View.VISIBLE);
                messageDate.setText(formattedDate);
            }
            CharSequence formattedTime = (String) DateFormat.format(timePattern,fullDate);
            messageTime.setText(formattedTime);
            messageText.setText(messaggio.getMessaggio());
        }
    }

    private boolean isDateChanged(long time1, long time2){
        boolean isChanged;
        if(time2 == 0){ // Ovvero se è il primo messaggio inserisci data del messaggio corrente
            isChanged = true;
        }else {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTimeInMillis(time1);
            cal2.setTimeInMillis(time2);

            boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                    cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);

            isChanged = !sameDay;
        }
        return isChanged;
    }

}
