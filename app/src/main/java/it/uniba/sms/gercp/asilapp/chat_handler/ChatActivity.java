package it.uniba.sms.gercp.asilapp.chat_handler;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import it.uniba.sms.gercp.asilapp.R;

public class ChatActivity extends AppCompatActivity {

    private ImageView sendButton;
    private EditText messageArea;
    private FloatingActionButton fabAutomaticScroll;
    private DatabaseReference mReference;
    private List<Messaggio> mMessageList;
    final String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    public static final int nMessaggiInvisibili = 5;
    static boolean fabHided = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mReference = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_messaggi) + userUID);

        sendButton = (ImageView)findViewById(R.id.send_button);
        messageArea = (EditText)findViewById(R.id.message_area);
        fabAutomaticScroll = (FloatingActionButton) findViewById(R.id.scroll_automatic);
        fabAutomaticScroll.setScaleX(0);
        fabAutomaticScroll.setScaleY(0);
        final AnimatorSet show_fab = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.show_fab);
        final AnimatorSet hide_fab = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.hide_fab);

        // Inizializzazione della recyclerView e dell'adapter.
        mMessageList = new ArrayList<>();
        final RecyclerView mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_chat);
        final LinearLayoutManager layManager = new LinearLayoutManager(this);
        final ChatAdapter mChatAdapter = new ChatAdapter(this, mMessageList);
        mMessageRecycler.setLayoutManager(layManager);
        mMessageRecycler.setAdapter(mChatAdapter);
        mMessageRecycler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMessageRecycler.scrollToPosition(mChatAdapter.getItemCount() - 1);
            }
        }, 400);

        // Comportamento FAB relativo allo scroll automatico fino all'ultimo messaggio della chat
        mMessageRecycler.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int scrollPosition = layManager.findLastVisibleItemPosition();
                if(scrollPosition <= (mChatAdapter.getItemCount() - nMessaggiInvisibili) && fabHided) {
                    show_fab.setTarget(fabAutomaticScroll);
                    show_fab.start();
                    fabHided = false;
                }else if(scrollPosition > (mChatAdapter.getItemCount() - nMessaggiInvisibili) && !fabHided){
                    hide_fab.setTarget(fabAutomaticScroll);
                    hide_fab.start();
                    fabHided = true;
                }
                Log.d("Scroll position", "last visible item position: " + scrollPosition);
            }
        });

        fabAutomaticScroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageRecycler.smoothScrollToPosition(mChatAdapter.getItemCount() - 1);
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageArea.getText().toString();
                sendMessage(messageText);
                hideKeyboard(ChatActivity.this);
                mMessageRecycler.post(new Runnable() {
                    @Override
                    public void run() {
                        mMessageRecycler.scrollToPosition(mChatAdapter.getItemCount() - 1);
                    }
                });
            }
        });

        // Se viene cliccato il pulsante Invia nella tastiera virtuale.
        messageArea.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    String messageText = messageArea.getText().toString();
                    sendMessage(messageText);
                    handled = true;
                }
                return handled;
            }
        });

        // Popolamento della lista di messaggi utilizzata dall'adapter, presente nel database.
        mReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                mMessageList.add(dataSnapshot.getValue(Messaggio.class));
                mChatAdapter.notifyItemChanged(mMessageList.size() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {}

        });
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void sendMessage(String messageText) {

        if(!messageText.equals("")){
            String centro = getString(R.string.id_centro);
            Messaggio messaggio = new Messaggio(messageText, userUID, centro);
            mReference.push().setValue(messaggio);

            /**
             * Implementazione TEMPORANEA di un messaggio di risposta automatico.
             * Nelle future versioni dove presente un UI lato centro di accoglienza sarà possibile
             * implementare una chat di risposta da parte del centro.
             */
            String testoMessaggioAutomatico = getString(R.string.messaggio_automatico);
            Messaggio messaggioAutomatico = new Messaggio(testoMessaggioAutomatico, centro, userUID);
            mReference.push().setValue(messaggioAutomatico);
            messageArea.setText("");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
