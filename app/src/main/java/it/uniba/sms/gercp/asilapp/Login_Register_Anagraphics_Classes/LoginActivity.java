package it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.transition.Fade;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.design.widget.Snackbar;

import it.uniba.sms.gercp.asilapp.MainActivity;
import it.uniba.sms.gercp.asilapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import android.view.inputmethod.EditorInfo;

public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final int REQUEST_READ_CONTACTS = 0;
    private static final String TAG = "AsilAppTAG";

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;

    private TextView app_name;
    private ImageView logo;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private NestedScrollView loginForm;

    private Button login, register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setExitTransition(new Fade());
        getWindow().setEnterTransition(new Fade());

        //logo e nome dell'app a inizio pagina
        app_name = findViewById(R.id.appName);
        logo = findViewById(R.id.logo);
        loginForm = findViewById(R.id.login_layout);


        // Animazione sul logo: tutte le animazioni sono in res->anim
        Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotation_anim);
        startRotateAnimation.setStartOffset(250);
        logo.startAnimation(startRotateAnimation);

        // Animazione sul nome dell'app
        Animation startFadeAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fading_anim_offset);
        app_name.startAnimation(startFadeAnimation);

        // Animazione d'ingresso nell'activity
        Animation startFadeAnimationActivity = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fading_anim_activity);
        loginForm.startAnimation(startFadeAnimationActivity);

        // Set up form di login
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        //populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);

        // Se viene cliccato il pulsante Accedi nella tastiera virtuale
        mPasswordView.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    attemptLogin();
                    handled = true;
                }
                return handled;
            }
        });


        //Button del LOGIN
        login = (Button) findViewById(R.id.email_sign_in_button);
        login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        //Button del REGISTER
        register = (Button) findViewById(R.id.registerButt);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goRegister();
            }
        });

        login.setEnabled(true);
        register.setEnabled(true);

        //Progress view di caricamento
        progressBar = findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();
    }

    //se viene cliccato il bottone registrati
    private  void goRegister(){
        Intent register = new Intent(getApplicationContext(),RegisterActivity.class);
        startActivity(register, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    //se viene cliccato il bottone login
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt

            if(!TextUtils.isEmpty(mEmailView.getText().toString()) &&
                 !TextUtils.isEmpty(mPasswordView.getText().toString()))   {

                login(email,password);

            }
        }
    }


    @SuppressLint("WrongConstant")
    private void login(String username, String password){

        login.setEnabled(false);
        register.setEnabled(false);

        progressBar.setVisibility(View.VISIBLE);
        //authenticate user
        mAuth.signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.

                        if (!task.isSuccessful()) {
                            // there was an error
                            progressBar.setVisibility(View.INVISIBLE);
                            Snackbar.make(findViewById(R.id.login_layout), "Authentication Failed", Snackbar.LENGTH_LONG).show();
                            login.setEnabled(true);
                            register.setEnabled(true);
                        } else {
                            checkIfEmailVerified();
                        }
                    }
                });

    }
    private void checkIfEmailVerified(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user.isEmailVerified())
        {
            progressBar.setVisibility(View.INVISIBLE);
            Snackbar.make(findViewById(R.id.login_layout), getString(R.string.auth_success), Snackbar.LENGTH_LONG).show();
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            // email is not verified, so just prompt the message to the user and restart this activity.
            // NOTE: don't forget to log out the user.
            Snackbar.make(findViewById(R.id.login_layout), getString(R.string.auth_failed), Snackbar.LENGTH_LONG).show();
            progressBar.setVisibility(View.INVISIBLE);
            FirebaseAuth.getInstance().signOut();

            //restart this activity
            this.recreate();

        }
    }

    //ovviamente una mail valida contiene almeno una @
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    //se la password è più lunga di 4 caratteri
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


}

