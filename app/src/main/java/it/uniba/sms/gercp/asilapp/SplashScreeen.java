package it.uniba.sms.gercp.asilapp;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;



public class SplashScreeen extends AppCompatActivity {

    private static int SPLASH = 2500; // Splash screen timer
    private Animation cerchioRotation;
    private Animation androidZoomInAnimation;
    private Animation androidZoomOutAnimation;
    private Animation fadingAppname;
    private ImageView logo;
    private ImageView cerchio;
    private TextView appname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        logo = findViewById(R.id.splash_logo);
        cerchio = findViewById(R.id.cerchio);
        cerchio.setVisibility(View.INVISIBLE);
        appname = findViewById(R.id.splash_appname);
        appname.setVisibility(View.INVISIBLE);

        cerchioRotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotation_cerchio_anim);
        androidZoomInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in_anim);
        androidZoomOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out_anim);
        fadingAppname = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fading_anim_activity);
        fadingAppname.setStartOffset(500L);

        androidZoomInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                logo.setAnimation(androidZoomOutAnimation);
                appname.setVisibility(View.VISIBLE);
                appname.startAnimation(fadingAppname);
                cerchio.startAnimation(cerchioRotation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreeen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH);
    }

    @Override
    protected void onStart() {
        super.onStart();
        logo.startAnimation(androidZoomInAnimation);
    }
}
