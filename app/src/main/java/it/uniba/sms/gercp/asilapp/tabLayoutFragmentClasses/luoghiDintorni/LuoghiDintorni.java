package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.luoghiDintorni;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniba.sms.gercp.asilapp.R;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria.SchedaRecyclerAdapter;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria.SchedaSanitariaClass;

public class LuoghiDintorni extends Fragment {

    private ProgressBar progressBar;
    private DatabaseReference mDatabase ;
    private List<LuogoClass> list;
    protected ArrayList<Uri> images = new ArrayList<>();
    private RecyclerView mRecycleView;
    private LuogoAdapter recyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.luoghi_dintorni, container, false);

        progressBar = rootView.findViewById(R.id.progressLuogo);
        mRecycleView = rootView.findViewById(R.id.reyclerview_luoghi);

        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_luoghi));
        list = new ArrayList<>();

        refreshRecycle();

        mDatabase.child("luoghi").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();

                for (DataSnapshot x : dataSnapshot.getChildren()) {

                    LuogoClass luogo = x.getValue(LuogoClass.class);
                    list.add(luogo);

                }
                Log.d("LUOGOVERIFICA",list.get(1).getNome_luogo());
                progressBar.setVisibility(View.INVISIBLE);
                recyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return rootView;
    }

    private void scarica_immagini(){
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        storageReference.child("images/fotoLuoghi/luogo_1.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        images.add(uri);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(),"Connection Error",Toast.LENGTH_LONG).show();
                    }
                });

        storageReference.child("images/fotoLuoghi/luogo_2.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        images.add(uri);
                    }
                });

        storageReference.child("images/fotoLuoghi/luogo_3.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        images.add(uri);
                    }
                });
        storageReference.child("images/fotoLuoghi/luogo_4.jpg").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        images.add(uri);
                    }
                });

    }

    private void refreshRecycle(){
        //In scarica immagini, non so per quale motivo, l'array viene popolato solo in locale (nel SuccessListner), all'uscita torna vuoto
        //scarica_immagini();
        images.add(Uri.parse("https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/images%2FfotoLuoghi%2Fluogo_1.jpg?alt=media&token=d3ddeecc-5ef3-45c4-ac0b-afe087311a17"));
        images.add(Uri.parse("https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/images%2FfotoLuoghi%2Fluogo_2.jpg?alt=media&token=0c4f3c04-0030-495d-a2ce-4d8f5c9a8e56"));
        images.add(Uri.parse("https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/images%2FfotoLuoghi%2Fluogo_3.jpg?alt=media&token=524c8429-8f33-4918-9150-eb607a0d1add"));
        images.add(Uri.parse("https://firebasestorage.googleapis.com/v0/b/asilapp-1bb2e.appspot.com/o/images%2FfotoLuoghi%2Fluogo_4.jpg?alt=media&token=79fa8529-d329-4f23-9106-d0574d96ce88"));
        final LinearLayoutManager layManager = new LinearLayoutManager(getActivity());
        mRecycleView.setLayoutManager(layManager);
        recyclerAdapter = new LuogoAdapter(list,images, getActivity());
        mRecycleView.setItemAnimator( new DefaultItemAnimator());
        mRecycleView.setAdapter(recyclerAdapter);
        layManager.setStackFromEnd(false);

    }
}
