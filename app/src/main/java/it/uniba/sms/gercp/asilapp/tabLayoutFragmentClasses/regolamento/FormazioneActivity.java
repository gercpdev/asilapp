package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento;

import android.content.Intent;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;


import java.io.File;
import it.uniba.sms.gercp.asilapp.R;

public class FormazioneActivity extends AppCompatActivity {

    final String URLManuali = "gs://asilapp-1bb2e.appspot.com/documenti/formazione/manualiApparecchi/";

    private Button manuale1,manuale2,manuale3, videoDisponibili;
    private ProgressBar progressBar1, progressBar2, progressBar3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formazione);

        getWindow().setEnterTransition(new Explode());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        manuale1 = findViewById(R.id.manuale1Butt);
        manuale2 = findViewById(R.id.manuale2Butt);
        manuale3 = findViewById(R.id.manuale3Butt);

        progressBar1 = findViewById(R.id.documento1ProgressBar);
        progressBar2 = findViewById(R.id.documento2ProgressBar);
        progressBar3 = findViewById(R.id.documento3ProgressBar);

        manuale1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar1.setVisibility(View.VISIBLE);
                downloadFile(URLManuali,"MisuratorePressione1.pdf");
            }
        });

        manuale2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar2.setVisibility(View.VISIBLE);
                downloadFile(URLManuali,"MisuratorePressione2.pdf");
            }
        });

        manuale3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar3.setVisibility(View.VISIBLE);
                downloadFile(URLManuali,"MisuratorePressione3.pdf");
            }
        });

        videoDisponibili = findViewById(R.id.videoDisponibili);
        videoDisponibili.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent video_disponibili = new Intent(getApplicationContext(), VideoDisponibiliDialog.class);
                startActivity(video_disponibili);
            }
        });





    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadFile(String URL,String file_name) {
        StorageReference storage = FirebaseStorage.getInstance().getReferenceFromUrl(URL);
        StorageReference target_file = storage.child(file_name);

        File rootPath = new File(Environment.getExternalStorageDirectory()+"/AsilApp/", "Documents");
        if(!rootPath.exists()) {
            rootPath.mkdirs();
        }

        Log.d("VERIFYPATH",rootPath.getAbsolutePath());

        final File localFile = new File(rootPath,file_name);


        target_file.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Snackbar.make(findViewById(R.id.activity_formazione),"Salvato in "+localFile.getAbsolutePath(),Snackbar.LENGTH_LONG).show();
                        progressBar1.setVisibility(View.INVISIBLE);
                        progressBar2.setVisibility(View.INVISIBLE);
                        progressBar3.setVisibility(View.INVISIBLE);
                    }})
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Snackbar.make(findViewById(R.id.activity_formazione),"Impossibile scaricare",Snackbar.LENGTH_LONG).show();
                        progressBar1.setVisibility(View.INVISIBLE);
                        progressBar2.setVisibility(View.INVISIBLE);
                        progressBar3.setVisibility(View.INVISIBLE);
                    }})
                .addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressBar1.setProgress((int)progress);
                        progressBar2.setProgress((int)progress);
                        progressBar3.setProgress((int)progress);
                    }});
    }

}
