package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import it.uniba.sms.gercp.asilapp.R;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento.FormazioneActivity;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento.InformazioneActivity;

public class Regolamento extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.documenti, container, false);

        Button accessoAreaFormativa = rootView.findViewById(R.id.formazioneAccesso);
        accessoAreaFormativa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent accesso = new Intent(getActivity(),FormazioneActivity.class);
                startActivity(accesso, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });

        Button accessoAreaInformativa = rootView.findViewById(R.id.informazioneAccesso);
        accessoAreaInformativa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent accesso = new Intent(getActivity(),InformazioneActivity.class);
                startActivity(accesso, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });

        return rootView;
    }
}
