package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.luoghiDintorni;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import it.uniba.sms.gercp.asilapp.R;


public class LuogoAdapter extends RecyclerView.Adapter<LuogoAdapter.MyHoder>{

    private List<LuogoClass> list;
    private List<Uri> images;
    private Context context;

    public LuogoAdapter(List<LuogoClass> list, List<Uri> images, Context context) {
        this.list = list;
        this.images = images;
        this.context = context;
    }

    @Override
    public MyHoder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.luogo_cardview,parent,false);
        MyHoder myHoder = new MyHoder(view);
        return myHoder;
    }

    @Override
    public void onBindViewHolder(MyHoder holder, int position) {
        final LuogoClass mylist = list.get(position);
        Glide.with(context)
                .load(images.get(position).toString())
                .into(holder.imageView);
        holder.nome.setText(mylist.getNome_luogo());
        holder.indirizzo.setText(mylist.getIndirizzo());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=1600 "+mylist.getNome_luogo()+","+"Bari, Italia");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        int arr = 0;

        try{
            if(list.size()==0){
                arr = 0;
            }
            else{
                arr=list.size();
            }

        }catch (Exception e){

        }

        return arr;
    }

    class MyHoder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView nome, indirizzo;

        public MyHoder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.luogo_foto);
            nome      = itemView.findViewById(R.id.luogo_name);
            indirizzo = itemView.findViewById(R.id.luogo_indirizzo);
        }
    }

}