package it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import it.uniba.sms.gercp.asilapp.R;

public class AnagraficaActivity extends AppCompatActivity {

    private final static String TAG = "AsilAppTAG";

    private TextView app_name;
    private ImageView logo;
    public static TextView data_nascita;
    private EditText stato_provenienza, citta_provenienza, num_telefono;
    private ImageButton scatta_foto;
    private Button register, modify;
    private Uri uri_foto;

    private StorageReference storageReference;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anagrafica);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti));
        storageReference = FirebaseStorage.getInstance().getReference();
        final String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        modify = findViewById(R.id.modify_butt);
        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent modificaAnagrafica = new Intent(getApplicationContext(), ModificaAnagraficaActivity.class);
                startActivity(modificaAnagrafica);
            }
        });

        final TextView nome = (TextView) findViewById(R.id.display_name);
        final TextView cognome = (TextView) findViewById(R.id.display_cognome);
        final RadioButton maschio = (RadioButton) findViewById(R.id.display_genderM);
        final RadioButton femmina = (RadioButton) findViewById(R.id.display_genderF);
        final TextView data = (TextView) findViewById(R.id.display_data_nasc);
        final TextView citta_provenienza = (TextView) findViewById(R.id.display_citta_provenienza);
        final TextView stato_provenienza = (TextView) findViewById(R.id.display_stato_provenienza);
        final TextView num_telefono = (TextView) findViewById(R.id.display_num_telefonico);
        final ImageView foto_profilo = (ImageView) findViewById(R.id.profile_picture);

        final ProgressBar progressBar = findViewById(R.id.progressProfilePicture);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                RichiedenteAsilo user;
                DataSnapshot child_dataSnapshot = dataSnapshot.child(userUID);
                user = child_dataSnapshot.getValue(RichiedenteAsilo.class);

                nome.setText(user.getNome());
                cognome.setText(user.getCognome());

                if(user.getSesso().equals("M")){
                    maschio.setChecked(true);
                    femmina.setChecked(false);
                }else{
                    maschio.setChecked(false);
                    femmina.setChecked(true);
                }
                if(user.getData_nascita().equals("")){
                   data.setText(R.string.campi_vuoti);
                }else{
                   data.setText(user.getData_nascita());
                }
                if(user.getCitta_provenienza().equals("")){
                    citta_provenienza.setText(R.string.campi_vuoti);
                }else{
                    citta_provenienza.setText(user.getCitta_provenienza());
                }
                if(user.getStato_provenienza().equals("")){
                    stato_provenienza.setText(R.string.campi_vuoti);
                }else{
                    stato_provenienza.setText(user.getStato_provenienza().toUpperCase());
                }
                if(user.getNum_telefono().equals("")){
                    num_telefono.setText(R.string.campi_vuoti);
                }else{
                    num_telefono.setText(user.getNum_telefono());
                }
                if((user.getIMG_URL()) != null){
                    //SE L'IMMAGINE E' GIA' IN MEMORIA E' INUTILE SCARICARLA, LA PRENDO IN OFFLINE
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory() + "/AsilApp" + "/Profile Pictures"+
                            "/" + user.getIMG_URL());

                    File imageStored = new File(uri.getPath());
                    if(imageStored.exists()) {
                        Log.d("PICVERIFY", "caricata da memoria");
                        foto_profilo.setImageURI(uri);
                    }else {
                        progressBar.setVisibility(View.VISIBLE);
                        storageReference.child("images/" + user.getIMG_URL()).getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Glide.with(getApplicationContext())
                                                .load(uri.toString())
                                                .into(foto_profilo);
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Snackbar.make(findViewById(R.id.anagrafica_layout),"Internet Error",Snackbar.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
