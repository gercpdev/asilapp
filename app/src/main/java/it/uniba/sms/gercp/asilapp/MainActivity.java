package it.uniba.sms.gercp.asilapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes.AnagraficaActivity;
import it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes.LoginActivity;
import it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes.RichiedenteAsilo;
import it.uniba.sms.gercp.asilapp.SettingsActivities.SettingsActivity;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.tabsAnimation.DepthPageTransformer;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.centroAccoglienza.CentroAccoglienza;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.luoghiDintorni.LuoghiDintorni;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.regolamento.Regolamento;
import it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria.SchedaSanitaria;

public class MainActivity extends AppCompatActivity{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private StorageReference storageReference;

    public static final String TAG = "AsilAppTAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti));
        auth = FirebaseAuth.getInstance(); //get current user
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {

                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();

                }else{
                    //Log.d("IDVERIFICA","MainID "+user.getUid());
                    initResources();

                }
            }
        };
    }


    public void initResources() {
        //Crea e associa la toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* !!! Crea e associa ViewPager e ViewPagerAdapter !!!*/
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        /* !!! Animazione trasparenza transizione tab !!!*/
        mViewPager.setPageTransformer(true, new DepthPageTransformer());

        /* !!! Crea e associa il tablayout !!!*/
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


        /* !!! Crea e associa il navigationDrawerMenu !!! */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // !!! In questo metodo viene specificata ogni azione compiuta su gli item del menu tipo login, settings, ... !!!
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.Profile:
                        Intent profile = new Intent(getApplicationContext(), AnagraficaActivity.class);
                        startActivity(profile);
                        break;

                    case R.id.Rate:
                        onBackPressed();
                        Intent rate = new Intent(getApplicationContext(), RateActivityDialog.class);
                        startActivity(rate);
                        break;

                        case R.id.Settings:
                        Intent settings = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(settings);
                        break;

                    case R.id.LogOut:
                        auth.signOut();
                        break;

                }
                return false;
            }
        });


        // Popolata la navigation bar
        final String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final View headerView = navigationView.getHeaderView(0);
        final TextView mail_menu = headerView.findViewById(R.id.email_menu);
        final TextView user_name_menu = headerView.findViewById(R.id.user_name_menu);
        final ImageView foto_profilo_menu = headerView.findViewById(R.id.foto_profilo_menu);
        final ProgressBar progressBar = headerView.findViewById(R.id.progressProfilePicture_menu);
        progressBar.setVisibility(View.VISIBLE);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                RichiedenteAsilo user;
                DataSnapshot child_dataSnapshot = dataSnapshot.child(userUID);
                user = child_dataSnapshot.getValue(RichiedenteAsilo.class);

                Uri uri = Uri.parse(Environment.getExternalStorageDirectory() + "/AsilApp" + "/Profile Pictures"+
                        "/" + user.getIMG_URL());

                File imageStored = new File(uri.getPath()); //STESSA COSA DI ANAGRAFICA, VEDO SE L IMG STA IN MEMORIA
                if(imageStored.exists()) {
                    //Log.d("PICVERIFY", "caricata da memoria");
                    foto_profilo_menu.setImageURI(uri);
                }else {
                    storageReference = FirebaseStorage.getInstance().getReference();
                    storageReference.child("images/"+user.getIMG_URL()).getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Glide.with(getApplicationContext())
                                            .load(uri.toString())
                                            .into(foto_profilo_menu);
                                }
                            });
                }
                mail_menu.setText(user.getMail());
                user_name_menu.setText(user.getNome()+ " " +user.getCognome());
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "loadUser:onCancelled", databaseError.toException());
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        auth.addAuthStateListener(authListener); //se il login è salvato accede direttamente all'app
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }


    //Questo metodo si occupa di definire cosa viene visualizzato nei vari fragment del tabLayout
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    return new SchedaSanitaria();
                case 1:
                    return new CentroAccoglienza();
                case 2:
                    return new LuoghiDintorni();
                case 3:
                    return new Regolamento();

            }

        return null;
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }
    }
}
