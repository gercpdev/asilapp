package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import it.uniba.sms.gercp.asilapp.R;

@SuppressLint("ValidFragment")
public class SchedaSanitaria extends Fragment {

    private FloatingActionButton fab_add;
    private ProgressBar progressBar;
    private DatabaseReference mDatabase ;
    private List<SchedaSanitariaClass> list;
    private RecyclerView mRecycleView;
    private SchedaRecyclerAdapter recyclerAdapter;
    private TextView nessunaScheda;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.scheda_sanitaria, container, false);

        fab_add = rootView.findViewById(R.id.fab2);
        progressBar = rootView.findViewById(R.id.progressSchedaSanitaria);
        mRecycleView = rootView.findViewById(R.id.reyclerview_scheda);
        nessunaScheda = rootView.findViewById(R.id.nessuna_scheda);

        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti));
        String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        list = new ArrayList<>();

        mDatabase.child(userUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DataSnapshot child_dataSnapshot = dataSnapshot.child("SchedeSanitarie");
                list.clear();

                    for (DataSnapshot x : child_dataSnapshot.getChildren()) {

                        SchedaSanitariaClass schedaSanitaria = x.getValue(SchedaSanitariaClass.class);
                        list.add(schedaSanitaria);

                    }

                    if(list.isEmpty()){
                        //Snackbar.make(rootView,getString(R.string.scheda_fail),Snackbar.LENGTH_LONG).show();
                        nessunaScheda.setVisibility(View.VISIBLE);
                    }else {
                        nessunaScheda.setVisibility(View.GONE);
                    }
                //Log.d("LISTVERIFICA", list.get(0).getRespiro());
                    progressBar.setVisibility(View.INVISIBLE);
                    recyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        refreshRecycle();
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nuova_scheda = new Intent(getActivity().getApplicationContext(),SchedaSanitariaActivity.class);
                startActivity(nuova_scheda);
            }
        });

        return rootView;
    }


    private void refreshRecycle(){
        final LinearLayoutManager layManager = new LinearLayoutManager(getActivity());
        mRecycleView.setLayoutManager(layManager);
        recyclerAdapter = new SchedaRecyclerAdapter(list, getActivity());
        mRecycleView.setItemAnimator( new DefaultItemAnimator());
        mRecycleView.setAdapter(recyclerAdapter);
        layManager.setStackFromEnd(false);
    }
}
