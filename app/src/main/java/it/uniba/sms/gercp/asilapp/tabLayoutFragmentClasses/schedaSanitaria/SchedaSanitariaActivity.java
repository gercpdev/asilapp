package it.uniba.sms.gercp.asilapp.tabLayoutFragmentClasses.schedaSanitaria;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import it.uniba.sms.gercp.asilapp.MainActivity;
import it.uniba.sms.gercp.asilapp.R;

public class SchedaSanitariaActivity extends AppCompatActivity {

    private final static int REQUEST_DISCOVERABLED = 0;
    private final static String BT_MODULE_MAC = "00:14:03:06:44:61";
    private final static String READY_HANDSHAKE = "r";

    private TextView input_temperatura, input_peso, input_pMin, input_pMax, input_glicemia;
    private SeekBar peso, p_min, p_max, glicemia;
    private Spinner respiro;
    private Button invia;
    private ImageButton btButton;
    private String temp, pes, pMin, pMax, glic, resp;

    private Boolean flag = false;
    private byte buffer[];
    private int bufferPosition;
    private boolean stopThread;
    private OutputStream outputStream;
    private InputStream inputStream;

    BluetoothAdapter btAdapter;
    BluetoothSocket btSocket=null;
    BluetoothDevice btDevice=null;
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheda_sanitaria);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        input_temperatura = findViewById(R.id.input_temperatura);
        input_peso = findViewById(R.id.input_peso);
        input_pMin = findViewById(R.id.input_pMin);
        input_pMax = findViewById(R.id.input_pMax);
        input_glicemia = findViewById(R.id.input_glicemia);
        peso = findViewById(R.id.seekPeso);
        p_min = findViewById(R.id.seekPressMin);
        p_max = findViewById(R.id.seekPressMax);
        glicemia = findViewById(R.id.seekGlicemia);
        respiro = findViewById(R.id.spinnerRespiro);
        invia = findViewById(R.id.invia_scehdaButt);
        btButton = findViewById(R.id.btButton);

        setSeekPeso();
        setSeekPressMin();
        setSeekPressMax();
        setSeekGlicemia();
        setSpinnerRespiro();

        invia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isValidTemp())
                    sendNewSchedaSanitaria();
                else
                    Snackbar.make(findViewById(R.id.activity_scheda_sanitaria),getString(R.string.invalid_temp),Snackbar.LENGTH_LONG).show();
            }
        });

        btButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = goBluetooth();
                if(flag==true){
                    Snackbar.make(findViewById(R.id.activity_scheda_sanitaria),"Connected to HC-05 Module! Loading for temperature..",Snackbar.LENGTH_LONG).show();
                    spedisci_messaggio("r");
                    input_temperatura.setText("");
                    beginListenForData();
                }

            }
        });
    }

    private void setSeekPeso(){
        peso.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                input_peso.setText(getString(R.string.kg)+": "+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setSeekPressMin(){
        p_min.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                input_pMin.setText(getString(R.string.pressione_minima)+": "+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setSeekPressMax(){
        p_max.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                input_pMax.setText(getString(R.string.pressione_massima)+": "+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setSeekGlicemia(){
        glicemia.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                input_glicemia.setText(getString(R.string.glicemia)+": "+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setSpinnerRespiro(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_respiro_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        respiro.setAdapter(adapter);

        respiro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //resp =
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean isValidTemp(){
        boolean result = false;
        try {

            double temp = Double.parseDouble(input_temperatura.getText().toString());
            if(temp > 15.0 && temp < 42.0){
                result = true;
            }

        } catch(NumberFormatException nfe) {

        }
        return result;
    }

    public String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd_MM_yyyy");
        return mdformat.format(calendar.getTime());
    }

    private void sendNewSchedaSanitaria(){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti));
        String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String data = getCurrentDate();

        temp = input_temperatura.getText().toString();
        pes  = peso.getProgress()+"";
        pMin = p_min.getProgress()+"";
        pMax = p_max.getProgress()+"";
        glic = glicemia.getProgress()+"";
        resp = respiro.getSelectedItem().toString();

        //genero un timestamp da associare alla data come nodo del db per distinguerlo
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        Log.d("SchedaParameters","pam: "+data+ " "+temp+" "+pes+" "+resp);
        SchedaSanitariaClass scheda = new SchedaSanitariaClass(data,temp,pes,pMin,pMax,glic,resp);
        mDatabase.child(userUID).child("SchedeSanitarie").child(data+"_"+ts).setValue(scheda)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.activity_scheda_sanitaria),getString(R.string.scheda_ok),Snackbar.LENGTH_LONG).show();
                        Intent main = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(main);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(findViewById(R.id.activity_scheda_sanitaria),"Connection error",Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    private boolean goBluetooth(){
        boolean connected = false;
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        //controllo per vedere se il bluetooth è supportato
        if(btAdapter == null) {
            Snackbar.make(findViewById(R.id.activity_scheda_sanitaria), "Bluetooth non supportato!", Snackbar.LENGTH_SHORT).show();
            finish();
        }

        if(!btAdapter.isDiscovering()){
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(intent,REQUEST_DISCOVERABLED);
        }

        btDevice = btAdapter.getRemoteDevice(BT_MODULE_MAC); //prendo il MAC
        try {
            btSocket = btDevice.createInsecureRfcommSocketToServiceRecord(uuid); //connetto manualmente
        } catch (IOException e) {

        }
        try {
            btSocket.connect(); // CONNESSIONE EFFETTIVA

            connected = true;
            flag = true;
        } catch (IOException e) {
            Snackbar.make(findViewById(R.id.activity_scheda_sanitaria),"Error!",Snackbar.LENGTH_LONG).show();
        }

        try {
            outputStream = btSocket.getOutputStream();
            connected = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            inputStream = btSocket.getInputStream();
            connected = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return connected;
    }

    protected void spedisci_messaggio(String c){
        if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write(c.getBytes());
            }
            catch (IOException e)
            {

            }
        }
    }

    protected  void beginListenForData() {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread)
                {
                    try
                    {
                        int byteCount = inputStream.available();
                        if(byteCount > 0)
                        {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string=new String(rawBytes,"UTF-8");
                            handler.post(new Runnable() {
                                public void run()
                                {
                                    input_temperatura.append(string);
                                }
                            });

                        }
                    }
                    catch (IOException ex)
                    {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }
}
