package it.uniba.sms.gercp.asilapp.chat_handler;

import java.util.Date;

public class Messaggio {

    private String messaggio;
    private String mittente;
    private String destinatario;
    private long orario;

    Messaggio() {}

    /**
     * @param messaggio Il contenuto del messaggio inviato.
     * @param mittente L'utente che ha inviato il messaggio.
     * @param destinatario L'utente a cui il messaggio è diretto.
     *
     *
     */
    public Messaggio(String messaggio, String mittente, String destinatario) {
        this.messaggio = messaggio;
        this.mittente = mittente;
        this.destinatario = destinatario;
        orario = new Date().getTime();
    }

    public String getMessaggio() {
        return messaggio;
    }

    public String getMittente() {
        return mittente;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public long getOrario() {
        return orario;
    }

    public String toString() {
        return " messaggio: " + messaggio + " " + mittente + " " + destinatario + " " + orario;
    }
}
