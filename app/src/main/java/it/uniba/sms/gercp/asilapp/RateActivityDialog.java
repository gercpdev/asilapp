package it.uniba.sms.gercp.asilapp;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes.RichiedenteAsilo;

public class RateActivityDialog extends AppCompatActivity {

    private RatingBar ratingBar;
    private TextView ratingText;
    private DatabaseReference mReference;
    final String userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_dialog);

        mReference = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti) + "/" + userUID);

        ratingBar = (RatingBar) findViewById(R.id.rating_bar);
        ratingText = (TextView) findViewById(R.id.rating_text);
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                RichiedenteAsilo ra = dataSnapshot.getValue(RichiedenteAsilo.class);
                ra.getVotoApp();
                if(ratingBar != null) ratingBar.setRating(ra.getVotoApp());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(fromUser) {
                    ratingText.setVisibility(View.VISIBLE);

                    //Aggiorna l'attributo voto dell'utente sul database Firebase
                    Map<String, Object> votoUpdate = new HashMap<>();
                    votoUpdate.put("votoApp", rating);
                    mReference.updateChildren(votoUpdate);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent goBack = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(goBack);
                        }
                    },100);
                }
            }
        });
    }
}
