package it.uniba.sms.gercp.asilapp.Login_Register_Anagraphics_Classes;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.design.widget.Snackbar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import it.uniba.sms.gercp.asilapp.MainActivity;
import it.uniba.sms.gercp.asilapp.R;

public class ModificaAnagraficaActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int LOAD_IMAGE_REQUEST_CODE = 2;
    public static final int MULTIPLE_PERMISSIONS = 10;

    private TextView app_name;
    private ImageView logo;
    public static TextView data_nascita;
    private EditText stato_provenienza, citta_provenienza, num_telefono;
    private ImageView foto_profilo;
    private ImageButton scatta_foto, importa_foto;
    private Button modifica;
    private String userUID;
    private String fileName; //nome del file
    private Bitmap bmp_toStore;
    //SE NON VENGONO DICHIARATE COSI' L INTENT NON PARTE
    protected String photoPath; //com.example. .../pictures/...
    protected Uri photoURI;
    protected InputStream stream;
    private static final String APP_INTERNAL_STORAGE = Environment.getExternalStorageDirectory() + "/AsilApp" +
            "/Profile Pictures";
    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    private DatabaseReference mDatabase;
    protected FirebaseUser user;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_anagrafica);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        user = FirebaseAuth.getInstance().getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();

        app_name = findViewById(R.id.appName);
        logo = findViewById(R.id.logo);
        Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotation_anim);
        logo.startAnimation(startRotateAnimation);
        Animation startFadeAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fading_anim_offset);
        app_name.startAnimation(startFadeAnimation);


        citta_provenienza = findViewById(R.id.input_citta_provenienza);
        stato_provenienza = findViewById(R.id.input_stato_provenienza);
        num_telefono = findViewById(R.id.num_telefonico);

        data_nascita = findViewById(R.id.data_nasc);
        data_nascita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attivaDatePicker();
            }
        });

        foto_profilo = findViewById(R.id.foto_profilo);

        scatta_foto = findViewById(R.id.fotoButt);
        scatta_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attivaCamera();
            }
        });

        importa_foto = findViewById(R.id.galleryButt);
        importa_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });
        modifica = findViewById(R.id.modificaButt);
        modifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToFirebase();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        //PROCEDURA PER SCATTARE UNA FOTO DALLA CAMERA
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            foto_profilo.setImageURI(photoURI);
            Bitmap bitmap1 = ((BitmapDrawable) foto_profilo.getDrawable()).getBitmap();
            bmp_toStore = Bitmap.createScaledBitmap(bitmap1, bitmap1.getWidth() / 3, bitmap1.getHeight() / 3, true);

            if (checkPermissions()) {
                photoURI = saveFile(bmp_toStore, fileName, APP_INTERNAL_STORAGE);
            }
            Snackbar.make(findViewById(R.id.modifica_anagrafica_layout), "Foto salvata in galleria", Snackbar.LENGTH_SHORT).show();
        }

        //PROCEDURA PER IMPORTARE UNA FOTO DALLA GALLERIA (copio l immagine nella cartella asil app)
        if (requestCode == LOAD_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(uri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Bitmap imgCompressed = Bitmap.createScaledBitmap(selectedImage, selectedImage.getWidth() / 3,
                        selectedImage.getHeight() / 3, true);
                foto_profilo.setImageBitmap(imgCompressed);

                File photoFile = null;
                photoFile = createImageFile();
                if (photoFile != null) {
                    photoURI = saveFile(selectedImage, fileName, APP_INTERNAL_STORAGE);
                    Log.d("URIPATH", uri.getPath());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                    saveFile(bmp_toStore, fileName, APP_INTERNAL_STORAGE);
                }
            }
        }
    }

    public void attivaDatePicker() {
        DialogFragment newFragment = new ModificaAnagraficaActivity.DateFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, LOAD_IMAGE_REQUEST_CODE);
    }

    public void attivaCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error
        }
        if (photoFile != null) {

            photoURI = FileProvider.getUriForFile(this,
                    "com.example.android.fileprovider",
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //timeStamp.substring(0,19);
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        photoPath = image.getAbsolutePath();
        fileName = image.getName(); //ritorna JPEG_AAAAMMGG_HHMMSS_1234567.. .jpg
        Log.d("VERIFICA", image.getName());
        return image;
    }

    public static class DateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR) - 25; //per farlo partire dagli anni 90
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(Objects.requireNonNull(getActivity()), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            data_nascita.setText(day + "/" + (month + 1) + "/" + year);
        }
    }

    public void sendToFirebase() {
        mDatabase = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_db_utenti));
        userUID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        mDatabase.child(userUID).child("data_nascita").setValue(data_nascita.getText().toString());
        mDatabase.child(userUID).child("citta_provenienza").setValue(citta_provenienza.getText().toString());
        mDatabase.child(userUID).child("stato_provenienza").setValue(stato_provenienza.getText().toString());
        mDatabase.child(userUID).child("num_telefono").setValue(num_telefono.getText().toString());
        mDatabase.child(userUID).child("img_URL").setValue(fileName);

        final ProgressBar progressBar = findViewById(R.id.progressBarModificaFoto); //non torna alla main finchè non carica
        TextView status_uploading = findViewById(R.id.aggiornamentoFoto);      //la foto su firebase
        progressBar.setVisibility(View.VISIBLE);
        status_uploading.setText(getString(R.string.caricamento));

        if(photoURI != null) {
            try {
                stream = new FileInputStream(new File(photoURI.getPath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            UploadTask uploadTask = storageReference.child("images").child(photoURI.getLastPathSegment()).putStream(stream);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressBar.setVisibility(View.INVISIBLE);
                    Snackbar.make(findViewById(R.id.modifica_anagrafica_layout), getString(R.string.dati_aggiornati), Snackbar.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent comeBackMain = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(comeBackMain);
                        }
                    },1000);
                }
            });
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            Snackbar.make(findViewById(R.id.modifica_anagrafica_layout), getString(R.string.dati_aggiornati), Snackbar.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent comeBackMain = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(comeBackMain);
                }
            },1000);
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this.getApplicationContext(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private Uri saveFile(Bitmap bitmap, String fileName, String path) {
        Uri uri = null;

        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdir();
        }

        Log.d("SAVE_IMAGE", folder.getAbsolutePath());

        File mypath = new File(folder, fileName);
        uri = Uri.parse(mypath.getAbsolutePath());

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.Media.MIME_TYPE, fileName);
            values.put(MediaStore.MediaColumns.DATA, folder.getAbsolutePath());

            getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(mypath));
            sendBroadcast(intent);
            fos.close();
        } catch (Exception e) {
            //Log.e("SAVE_IMAGE", e.getMessage(), e);
        }
        return uri;
    }
}


    /*  !!!!!!!!!!!!!!!   LEGGIMI   !!!!!!!!!!!!!!!!

                             La parte relativa alla foto funziona cosi:
                             ci sono due variabili contenitore
                                photoURI : contiene il path dell'immagine da spedire
                                stream : il buffer che prende in input il path e crea un file

        caso 1) SCATTO DELLA FOTO

        attivacamera(): genera l'intent e crea un file provvisorio vuoto che conterra' il nome del file
        onResult(): riempio il file vuoto di prima con l URI dell immagine scattata, compressa per salvare MB,
                    e creo una copia accessibile dalla galleria.
                    in tot. i file saranno due(uguali),uno memorizzato privatamente nel com. ... dell app
                    e uno visibile in galleria memorizzato in storage/AsilApp/Profile Pictures

                    alla fine photoURI contiene il path dell img in galleria.

         caso 2) IMPORT DA GALLERIA

         openGallery(): genera l'intent della galleria, prende l' URI dell img selezionata, fa una copia privata in
                        com. ... e carica lo stream in onResult()


     */